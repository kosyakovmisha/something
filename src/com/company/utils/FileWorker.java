package com.company.utils;

import java.io.FileWriter;
import java.io.IOException;

public class FileWorker {

    public static void write(String fileName, String stringBuffer) throws IOException {

        try (FileWriter fw = new FileWriter(fileName, false)) {
            fw.write(stringBuffer);
        }
    }
}
