package com.company.utils.graphics;

import com.company.models.World;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StatisticWindow {

    private World world;

    public StatisticWindow(World world) {
        JFrame frame = new JFrame();
        this.world = world;
        JButton btn_start = new JButton("step");
        btn_start.setBounds(15, 15, 100, 50);

        frame.add(btn_start);
        frame.setSize(500, 500);
        frame.setLocation(600, 1);
        frame.setResizable(false);
        frame.setLayout(null);
        frame.setVisible(true);

        btn_start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });
    }
}
