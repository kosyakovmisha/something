package com.company.utils.graphics;

import com.company.models.objects.Field;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;

public class GraphicWindow extends Frame {

    private Field[][] map;

    public GraphicWindow(Field[][] map) {

        super("Something 01");
        setSize(500, 500);
        setVisible(true);
        setResizable(false);
        setLocation(1, 1);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
                System.exit(0);
            }
        });

        this.map = map;
    }

    public void paint(Graphics graphics) {
        int x, y;
        Graphics2D g2d = (Graphics2D) graphics;
        for(int i = 0; i < 50; i++) {
            for(int j = 0; j < 50; j++) {
                x = i * 10;
                y = j * 10;
                g2d.setColor(Color.gray);
                Shape borderField = new Rectangle2D.Double(x, y, 10, 10);
                g2d.draw(borderField);
                Shape field = new Rectangle2D.Double(x + 1, y + 1, 8, 8);
                g2d.draw(field);
                if(map[i][j].getState() != null) {
                    g2d.setColor(Color.GREEN);
                } else {
                    g2d.setColor(Color.WHITE);
                }
                g2d.fill(field);
            }
        }
    }
}
