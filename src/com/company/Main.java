package com.company;

import com.company.models.World;
import com.company.utils.FileWorker;
import com.company.utils.graphics.GraphicWindow;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        World world = new World();
        try {
            FileWorker.write("note.txt", world.mapToString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
