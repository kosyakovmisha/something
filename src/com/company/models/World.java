package com.company.models;

import com.company.models.objects.Essence;
import com.company.models.objects.Field;
import com.company.utils.graphics.GraphicWindow;
import com.company.utils.graphics.StatisticWindow;

import java.util.Random;

public class World implements IWorld{

    private Field[][] map = new Field[HEIGHT][WIDTH];

    private void initMap() {
        Random random = new Random();
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                map[i][j] = new Field();
                if (random.nextBoolean()) {
                    map[i][j].setState(new Essence());
                }
            }
        }
        showMap();
        new StatisticWindow(this);
    }

    public String mapToString() {

        StringBuilder mapString = new StringBuilder();
        for(int i = 0; i < HEIGHT; i++) {
            for(int j = 0; j < WIDTH; j++) {
                String temp = "'" + i + ", " + j + "'" + " : " + "'";
                mapString.append(temp);
                if(map[i][j].getState() != null) {
                    mapString.append(map[i][j].getState().getId());
                } else {
                    mapString.append("null");
                }
                mapString.append("'\n");
            }
        }
        return mapString.toString();
    }

    public void showMap() {
       GraphicWindow window =  new GraphicWindow(map);

    }

    public Field[][] getMap() {
        return map;
    }

    public World() {
        initMap();
    }
}
