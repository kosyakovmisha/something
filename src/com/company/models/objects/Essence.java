package com.company.models.objects;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Essence {

    private static int count = 0;
    private static final int memorySize = 5;
    private static Map<Long, Essence> essenceMap = new HashMap<>();

    private long id;
    private String state;
    private long age;
    private LinkedList<String> memory;

    public static int getCount() {
        return count;
    }

    public long getId() {
        return id;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setMemory(LinkedList<String> memory) {
        this.memory = memory;
    }

    public LinkedList<String> getMemory() {
        return memory;
    }



    public Essence() {
        this.id = ++count;
        this.state = "wait";
        this.age = 0;
        this.memory = new LinkedList<>();
        essenceMap.put(this.id, this);
    }
}
