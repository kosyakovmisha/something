package com.company.models.objects;

public class Field {

    private Essence state;

    public void setState(Essence state) {
        this.state = state;
    }

    public Essence getState() {
        return state;
    }


    public Field() {

    }
}
